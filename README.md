# Basic Deal Invoice System

## Requirements
1. Java: openjdk64-11.0.9.1

## Snippets

1. Add multiple line items to an existing deal invoice record
```sh
db.ssp_deal_invoice.update({"_id" : ObjectId("5fdf3b64572f35ac0504662b") }, {$set: {"lineItems": [
    {
			"dealId" : "GD-00000-00025",
			"lineItemId" : "5fc8b68f4007872eaebec7a1",
			"lineItemName" : "Line item 001",
			"billboardNames" : [
				"Sunway City’s OOH"
			],
			"impressionType" : "SPOT",
			"cpmRate" : 100,
			"totalPotentialView" : 3450,
			"totalSpot" : 401,
			"totalEarning" : 773.5,
			"schedule" : {
				"type" : "ALL_TIME",
				"dayPreference" : [
					"SUNDAY",
					"MONDAY",
					"TUESDAY",
					"WEDNESDAY",
					"THURSDAY",
					"FRIDAY",
					"SATURDAY"
				],
				"startTime" : "07:00 AM",
				"endTime" : "06:00 AM"
			},
			"timeZoneId" : "Asia/Kuala_Lumpur",
			"startDate" : {
				"dayOfYear" : 338,
				"str" : "2020-12-03",
				"date" : "2020-12-03",
				"nformat" : 20201203
			},
			"endDate" : {
				"dayOfYear" : 31,
				"str" : "2020-12-31",
				"date" : "2021-01-01",
				"nformat" : 20201231
			}
		},
        {
			"dealId" : "GD-00000-00025",
			"lineItemId" : "5fc8b68f4007872eaebec7a1",
			"lineItemName" : "Line item 002",
			"billboardNames" : [
				"Sunway City’s OOH"
			],
			"impressionType" : "SPOT",
			"cpmRate" : 100,
			"totalPotentialView" : 3450,
			"totalSpot" : 401,
			"totalEarning" : 773.5,
			"schedule" : {
				"type" : "ALL_TIME",
				"dayPreference" : [
					"SUNDAY",
					"MONDAY",
					"TUESDAY",
					"WEDNESDAY",
					"THURSDAY",
					"FRIDAY",
					"SATURDAY"
				],
				"startTime" : "07:00 AM",
				"endTime" : "06:00 AM"
			},
			"timeZoneId" : "Asia/Kuala_Lumpur",
			"startDate" : {
				"dayOfYear" : 338,
				"str" : "2020-12-03",
				"date" : "2020-12-03",
				"nformat" : 20201203
			},
			"endDate" : {
				"dayOfYear" : 31,
				"str" : "2020-12-31",
				"date" : "2021-01-01",
				"nformat" : 20201231
			}
		},
        {
			"dealId" : "GD-00000-00025",
			"lineItemId" : "5fc8b68f4007872eaebec7a1",
			"lineItemName" : "Line item 003",
			"billboardNames" : [
				"Sunway City’s OOH"
			],
			"impressionType" : "SPOT",
			"cpmRate" : 100,
			"totalPotentialView" : 3450,
			"totalSpot" : 401,
			"totalEarning" : 773.5,
			"schedule" : {
				"type" : "ALL_TIME",
				"dayPreference" : [
					"SUNDAY",
					"MONDAY",
					"TUESDAY",
					"WEDNESDAY",
					"THURSDAY",
					"FRIDAY",
					"SATURDAY"
				],
				"startTime" : "07:00 AM",
				"endTime" : "06:00 AM"
			},
			"timeZoneId" : "Asia/Kuala_Lumpur",
			"startDate" : {
				"dayOfYear" : 338,
				"str" : "2020-12-03",
				"date" : "2020-12-03",
				"nformat" : 20201203
			},
			"endDate" : {
				"dayOfYear" : 31,
				"str" : "2020-12-31",
				"date" : "2021-01-01",
				"nformat" : 20201231
			}
		}
] } } )
```