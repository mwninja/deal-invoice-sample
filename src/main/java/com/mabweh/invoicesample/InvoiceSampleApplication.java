package com.mabweh.invoicesample;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Slf4j
@SpringBootApplication
public class InvoiceSampleApplication {

	private final Environment env;
	
	@PostConstruct
    public void initApplication() {
		System.out.println(env.getActiveProfiles());		
	}

	public InvoiceSampleApplication(Environment env) {
        this.env = env;
    }

	public static void main(String[] args) throws UnknownHostException {
		// SpringApplication.run(InvoiceSampleApplication.class, args);
		SpringApplication app = new SpringApplication(InvoiceSampleApplication.class);

        Environment env = app.run(args).getEnvironment();
        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        log.info("\n----------------------------------------------------------\n\t" +
                "Application '{}' is running! Access URLs:\n\t" +
                "Local: \t\t{}://localhost:{}\n\t" +
                "External: \t{}://{}:{}\n\t" +
                "Profile(s): \t{}\n----------------------------------------------------------",
            env.getProperty("spring.application.name"),
            protocol,
            env.getProperty("server.port"),
            protocol,
            InetAddress.getLocalHost().getHostAddress(),
            env.getProperty("server.port"),
            env.getActiveProfiles());
	}

}
