package com.mabweh.invoicesample.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.mabweh.invoicesample.domain.DealInvoice;

public interface DealInvoiceRepository extends MongoRepository<DealInvoice, String> {
    Optional<DealInvoice> findOneByInvoiceId(String invoiceId);
}
