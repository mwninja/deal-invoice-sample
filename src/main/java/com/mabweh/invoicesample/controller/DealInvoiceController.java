package com.mabweh.invoicesample.controller;

import com.itextpdf.text.DocumentException;
import com.mabweh.invoicesample.domain.DealInvoice;
import com.mabweh.invoicesample.repository.DealInvoiceRepository;
import com.mabweh.invoicesample.service.DealInvoiceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/deal-invoice")
public class DealInvoiceController {

    @Autowired
    private DealInvoiceRepository repository;

    @Autowired
    private DealInvoiceService dealInvoiceService;

    @GetMapping(value = "/{invoiceId}")
    public ResponseEntity<?> getDealInvoice(@PathVariable String invoiceId) {
        log.info("in getDealInvoice..");
        // deal-invoice/INV20201216084082981043666706
        Optional<DealInvoice> res = repository.findOneByInvoiceId(invoiceId);

        if (res.isPresent()) {
            return new ResponseEntity<>(res.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping(value = "/{invoiceId}/download")
    public ResponseEntity<InputStreamResource> downloadDealInvoice(@PathVariable String invoiceId) throws IOException, DocumentException  {

        Optional<DealInvoice> res = repository.findOneByInvoiceId(invoiceId);

        if (res.isPresent()) {
            // call service to prepare invoice pdf
            log.info("Generating deal invoice PDF..");
            
            ByteArrayInputStream bis = dealInvoiceService.prepareDealInvoicePDF(res.get());

            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", "inline; filename=test.pdf");

            return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


}