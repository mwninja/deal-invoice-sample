package com.mabweh.invoicesample.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;

@Builder
@Data
@EqualsAndHashCode()
public class DealInvoicePriceDTO implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String dealId;

    private BigDecimal subTotal;
    private String taxLabel;
    private double taxPercentage;
    private BigDecimal taxAmount; // calculate against subTotal
    private BigDecimal discountAmount;
    private double discountPercentage;
    private BigDecimal grandTotal;

}
