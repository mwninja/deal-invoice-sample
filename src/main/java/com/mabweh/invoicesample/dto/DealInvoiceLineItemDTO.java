package com.mabweh.invoicesample.dto;

import java.util.List;
import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import com.mabweh.invoicesample.enums.ImpressionType;

@Builder
@Data
@EqualsAndHashCode()
public class DealInvoiceLineItemDTO implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String dealId;

    private String lineItemId;
    private String lineItemName;
    private List<String> billboardNames;
    private ImpressionType impressionType;
    private double cpmRate; // from netRate
    private long totalPotentialView;
    private long totalSpot;
    private BigDecimal totalEarning;
    private ScheduleDTO schedule; // may need to convert this to a simpler start/end time
    private String timeZoneId;
    private CustomDate startDate;
    private CustomDate endDate;

}
