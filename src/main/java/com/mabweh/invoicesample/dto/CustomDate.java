package com.mabweh.invoicesample.dto;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.Data;

@Data
public class CustomDate implements Serializable {
    
    private static final long serialVersionUID = 1L;


    public CustomDate() {
    	// empty constructor
    }
    
    public CustomDate(LocalDate date) {
        String str = date.toString();
        int dayOfYear = date.getDayOfYear();
        String dayStr = date.getDayOfMonth() < 10 ? "0" + date.getDayOfMonth() : String.valueOf(date.getDayOfMonth());
        Long nFormat = Long.parseLong(String.format("%d%d%s", date.getYear(), date.getMonthValue(), dayStr));
    }
}