package com.mabweh.invoicesample.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class BuyerDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String dealId; // buyer deal id

    @NotNull(message="dsp is required.")
    private String dsp;
    
    @NotNull(message="name is required.")
    private String name;

    @NotNull(message="seatId is required.")
    private String seatId;

    private String buyerAccountId;

    public BuyerDTO() {
        // Empty constructor
    }
}