package com.mabweh.invoicesample.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;
import com.mabweh.invoicesample.util.ValueOfEnum;
import com.mabweh.invoicesample.enums.ScheduleType;

@Data
public class ScheduleRequestDTO {

    @NotNull(message="type is required.")
    @ValueOfEnum(enumClass = ScheduleType.class)
    private String type;

    private List<String> dayPreference;

    @NotNull(message="startTime is required.")
    private String startTime;

    @NotNull(message="endTime is required.")
	private String endTime;
}