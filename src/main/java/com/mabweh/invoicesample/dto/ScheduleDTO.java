package com.mabweh.invoicesample.dto;

import com.mabweh.invoicesample.enums.ScheduleType;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ScheduleDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    public ScheduleDTO() {
        //
    }

    public ScheduleDTO(ScheduleRequestDTO request) {
        ScheduleType type = ScheduleType.valueOf(request.getType());
        List<String> dayPreference = request.getDayPreference();
        String startTime = request.getStartTime();
        String endTime = request.getEndTime();
    }
}