package com.mabweh.invoicesample.dto;

import java.io.Serializable;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class SellerDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull(message="currency is required.")
    private String name;
    
    private String phone;
    private String email;
}