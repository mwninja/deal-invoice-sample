package com.mabweh.invoicesample.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DealInvoiceInsightsDTO implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String dealId;

    private BigDecimal totalEarning;
    private long totalNumberOfSpots; // impression
    private long totalNumberOfPotentialViews;
    private BigDecimal totalCpmRate;
    private int totalNumberOfScreens;
    private int totalNumberOfBillboards;

}
