package com.mabweh.invoicesample.enums;

public enum CreativeStatus {
    PENDING,
    ACTIVE,
    IN_ACTIVE,
    DELIVERING,
    COMPLETED,
    ARCHIVED,
    DEACTIVATED,
    APPROVED,
    REJECTED,
    PAUSED
    
}