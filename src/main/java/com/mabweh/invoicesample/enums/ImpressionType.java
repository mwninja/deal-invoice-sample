package com.mabweh.invoicesample.enums;

public enum ImpressionType {
    SPOT,
    AUDIENCE
}