package com.mabweh.invoicesample.enums;

public enum DealType {
    GUARANTEED,
    PREFERRED_DEAL,
    PRIVATE_AUCTION,
    PUBLIC_AUCTION,
    OPEN_AUCTION
}