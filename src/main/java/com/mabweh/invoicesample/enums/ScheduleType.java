package com.mabweh.invoicesample.enums;

public enum ScheduleType {
    ALL_TIME,
    ON_SCHEDULE
}