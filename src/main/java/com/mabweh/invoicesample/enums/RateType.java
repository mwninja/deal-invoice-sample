package com.mabweh.invoicesample.enums;

public enum RateType {
    CPD,
    CPM
}