package com.mabweh.invoicesample.enums;

public enum DealStatus {
    GENERATED,
    PENDING,
    APPROVED,
    REJECTED,
    PENDING_CREATIVES,
    LIVE,
    COMPLETED,
    IN_ACTIVE,
    REOPEN,
    ARCHIVED
}