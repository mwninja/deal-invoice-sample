package com.mabweh.invoicesample.service;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import com.mabweh.invoicesample.domain.DealInvoice;
import com.mabweh.invoicesample.enums.DealType;
import com.mabweh.invoicesample.dto.DealInvoiceInsightsDTO;
import com.mabweh.invoicesample.dto.DealInvoiceLineItemDTO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.RoundingMode;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Slf4j
@Service
@RequiredArgsConstructor
public class DealInvoiceService {

    private final ResourceLoader loader;

    private static final BaseColor BLACK_COLOUR = new BaseColor(0x00, 0x00, 0x00, 0xDE);
    private static final BaseColor WHITE_COLOUR = new BaseColor(0xFF, 0xFF, 0xFF);
    private static final BaseColor GREENISH_COLOUR = new BaseColor(30, 45, 67);
    private static final BaseColor GREEN_COLOUR = new BaseColor(43, 222, 145);
    private static final BaseColor RED_COLOUR = new BaseColor(250, 0, 0);
    private static final BaseColor GREYISH_COLOUR = new BaseColor(28, 46, 66);
    private static final BaseColor VERY_LIGHT_GREY_COLOUR = new BaseColor(113, 129, 144);
    private static final BaseColor DARK_CYANISH_COLOUR = new BaseColor(29, 45, 68);
    private static final BaseColor LIGHT_GREYISH_COLOUR = new BaseColor(229, 234, 228);
    private static final BaseColor BLUEISH_COLOUR = new BaseColor(63, 86, 175);
    private static final int XL_FONT_SIZE = 16;
    private static final int L_FONT_SIZE = 12;
    private static final int M_FONT_SIZE = 10;
    private static final int S_FONT_SIZE = 6;

    private PdfWriter pdfWriter;

    public ByteArrayInputStream prepareDealInvoicePDF(DealInvoice dealInvoice) throws IOException, DocumentException {

        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            pdfWriter = PdfWriter.getInstance(document, out);
            document.open();
            float yCoord = 570;

            // 1. Create header
            prepareHeader(pdfWriter, dealInvoice);

            // 2. Create deal summary top card
            prepareDealSummaryTopCard(pdfWriter, dealInvoice);

            // 3. Create line item/inventory table
            yCoord = createLineItemTable(document, pdfWriter, 20, yCoord, dealInvoice);

            // 1. Calculate if to add new page and reset yCoord
            if (yCoord < 300) {
                document.newPage();
                yCoord = document.top() - document.topMargin() - 30;
            }

            // 4. Create deal summary bottom card
            yCoord = prepareDealSummaryBottomCard(document, pdfWriter, 20, yCoord - 100, dealInvoice);

            // 5. Create terms and conditions card
            prepareTermsAndConditionsCard(document, pdfWriter, dealInvoice, 4, yCoord);

            document = addMetaData(document, dealInvoice);
            document.close();

        } catch (DocumentException | IOException ex) {
            log.info("DocumentException while preparing PDF");
            Logger.getLogger(DealInvoiceService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

    private void prepareHeader(PdfWriter writer, DealInvoice dealInvoice) throws DocumentException, IOException {
        float llx = 4; // left ness
        float lly = 730; // height ish
        float urx = 591; // width ish
        float ury = 838; // height ish

        // Add rectangle
        PdfContentByte canvas = writer.getDirectContent();
        Rectangle rect = new Rectangle(llx, lly, urx, ury);
        rect.setBorder(Rectangle.BOX);
        rect.setBorderWidth(1);
        rect.setBackgroundColor(GREENISH_COLOUR);
        canvas.rectangle(rect);

        // Add text to left start
        createCanvasTextHeader(canvas, XL_FONT_SIZE, llx + 30, lly + 55, "INSERTION ORDER", WHITE_COLOUR);
        createCanvasTextHeader(canvas, M_FONT_SIZE, llx + 30, lly + 30, "Invoice ID: " + dealInvoice.getInvoiceId(),
                WHITE_COLOUR);

        // Add LMX logo to top right
        Image image = getLmxImage();
        image.scaleAbsolute(100, 50);
        image.setAbsolutePosition(urx - 140, lly + 25);
        canvas.addImage(image);
    }

    private void prepareDealSummaryTopCard(PdfWriter writer, DealInvoice dealInvoice)
            throws DocumentException, IOException {
        PdfContentByte pcb = writer.getDirectContent();
        List<Map<String, String>> topSummaryKeyValue = prepareTopSummaryKeyValue(dealInvoice);

        if (!topSummaryKeyValue.isEmpty()) {
            createTopSummaryTable(pcb, topSummaryKeyValue.get(0), 34, 710);
            createTopSummaryTable(pcb, topSummaryKeyValue.get(1), 300, 710);
        }

    }

    private float prepareDealSummaryBottomCard(Document document, PdfWriter writer, float xCoord, float yCoord,
            DealInvoice dealInvoice) throws DocumentException, IOException {

        PdfContentByte canvas = writer.getDirectContent();
        Rectangle rect = new Rectangle(xCoord, yCoord, 570, yCoord - 170);
        rect.setBorder(Rectangle.NO_BORDER);
        rect.setBackgroundColor(DARK_CYANISH_COLOUR);
        // rect.setBackgroundColor(new BaseColor(58,72,92));
        canvas.rectangle(rect);

        // Text in summary card
        float columnTextYCoord = yCoord;
        Font headerFont = getFontRobotoForContent(L_FONT_SIZE, Font.BOLD, WHITE_COLOUR);
        Font contentFont1 = getFontRobotoForContent(M_FONT_SIZE, Font.BOLD, WHITE_COLOUR);
        Font contentFont2 = getFontRobotoForContent(M_FONT_SIZE, Font.NORMAL, WHITE_COLOUR);
        List<Map<String, String>> bottomSummaryData = prepareBottomSummaryKeyValue(dealInvoice);
        // left table-ish
        createColumnText(canvas, xCoord, yCoord, "Invoice Summary", headerFont);
        createColumnText(canvas, xCoord, columnTextYCoord - 30, "Preferred Deal", contentFont1);
        createBottomSummaryTable(canvas, xCoord, columnTextYCoord - 50, bottomSummaryData.get(0), contentFont2);
        // right table-ish
        createColumnText(canvas, xCoord + 280, columnTextYCoord, "Price Details", contentFont1);
        createBottomSummaryTable(canvas, xCoord + 280, columnTextYCoord - 20, bottomSummaryData.get(1), contentFont2);
        // bottom
        createColumnText(canvas, xCoord + 230, yCoord - 136, "Grand Total",
                getFontRobotoForContent(L_FONT_SIZE + 2, Font.BOLD, WHITE_COLOUR));
        String grandTotalText = String.format("%,.2f",
                dealInvoice.getPriceDetails().getGrandTotal().setScale(2, RoundingMode.DOWN));
        createColumnText(canvas, xCoord + 463, yCoord - 136, grandTotalText,
                getFontRobotoForContent(L_FONT_SIZE + 2, Font.BOLD, GREEN_COLOUR));

        // vertical line
        canvas.setColorStroke(VERY_LIGHT_GREY_COLOUR);
        canvas.moveTo(300, yCoord - 10);
        canvas.lineTo(300, yCoord - 130);
        // bottom horizontal line
        canvas.setColorStroke(VERY_LIGHT_GREY_COLOUR);
        canvas.moveTo(20, yCoord - 140);
        canvas.lineTo(570, yCoord - 140);
        // top left horizontal line
        canvas.setColorStroke(VERY_LIGHT_GREY_COLOUR);
        canvas.moveTo(20, yCoord - 30);
        canvas.lineTo(290, yCoord - 30);
        canvas.closePathStroke();

        // calculate if a need to add new page
        if (yCoord < 290) {
            document.newPage();
            yCoord = document.top() - document.topMargin();
        } else {
            // separator between summary table and terms and conditions
            canvas.setColorStroke(LIGHT_GREYISH_COLOUR);
            canvas.setLineWidth(0);
            canvas.moveTo(10, yCoord - 190);
            canvas.lineTo(580, yCoord - 190);
        }

        return yCoord;
    }

    private List<Map<String, String>> prepareBottomSummaryKeyValue(DealInvoice dealInvoice) {
        List<Map<String, String>> preparedData = new ArrayList<>();

        Map<String, String> leftItems = new LinkedHashMap<>();
        leftItems.put("Total Number of Billboards",
                dealInvoice.getInsightsDetails() != null
                        ? numOfBillboarsdAndScreenText(dealInvoice.getInsightsDetails())
                        : "");
        leftItems.put("Total Number of Spot",
                dealInvoice.getInsightsDetails() != null
                        ? String.format("%,d", dealInvoice.getInsightsDetails().getTotalNumberOfSpots())
                        : "");
        leftItems.put("Total Number of Potential View",
                dealInvoice.getInsightsDetails() != null
                        ? String.format("%,d", dealInvoice.getInsightsDetails().getTotalNumberOfPotentialViews())
                        : "");
        leftItems
                .put("Total CPM Rate",
                        dealInvoice.getInsightsDetails() != null
                                ? String.format("%,.2f",
                                        dealInvoice.getInsightsDetails().getTotalCpmRate().setScale(2,
                                                RoundingMode.DOWN))
                                        + " USD"
                                : "");

        Map<String, String> rightItems = new LinkedHashMap<>();
        String subTotalText = String.format("%,.2f",
                dealInvoice.getPriceDetails().getSubTotal().setScale(2, RoundingMode.DOWN));
        rightItems.put("Sub-Total", String.format("%s %s", subTotalText, dealInvoice.getCurrency()));
        String taxAmountText = String.format("%,.2f",
                dealInvoice.getPriceDetails().getTaxAmount().setScale(2, RoundingMode.DOWN));
        String taxLabelText = String.format("%s (%s)", dealInvoice.getPriceDetails().getTaxLabel(),
                String.valueOf(dealInvoice.getPriceDetails().getTaxPercentage()) + "%");
        rightItems.put(taxLabelText, taxAmountText);
        String discountLabelText = String.format("Discount (%s) :",
                String.valueOf(dealInvoice.getPriceDetails().getDiscountPercentage()) + "%");
        String discountAmountText = String.format("%,.2f",
                dealInvoice.getPriceDetails().getDiscountAmount().setScale(2, RoundingMode.DOWN));
        rightItems.put(discountLabelText, discountAmountText);

        preparedData.add(leftItems);
        preparedData.add(rightItems);
        return preparedData;
    }

    private float createBottomSummaryTable(PdfContentByte canvas, float xCoord, float yCoord,
            Map<String, String> fieldData, Font contentFont) throws DocumentException {
        for (Map.Entry<String, String> entry : fieldData.entrySet()) {
            createColumnText(canvas, xCoord, yCoord, entry.getKey(), contentFont);
            createColumnText(canvas, xCoord + 193, yCoord, entry.getValue(), contentFont);
            yCoord -= 15; // for new line spacing
        }

        return yCoord;
    }

    private String numOfBillboarsdAndScreenText(DealInvoiceInsightsDTO insightDetails) {
        return String.format("%s" + "(%s %s)", insightDetails.getTotalNumberOfBillboards(),
                insightDetails.getTotalNumberOfScreens(),
                insightDetails.getTotalNumberOfScreens() > 1 ? "screens" : "screen");
    }

    private void prepareTermsAndConditionsCard(Document document, PdfWriter writer, DealInvoice dealInvoice,
            float xCoord, float yCoord) throws DocumentException, IOException {
        // The ideal way - split the string by line
        Paragraph title = createParagraph("TERMS AND CONDITIONS",
                getFontRobotoForContent(L_FONT_SIZE, Font.BOLD, RED_COLOUR));
        document.add(addEmptyLine(title, 1));
        String[] termsAndConditions = dealInvoice.getTermsAndConditions().split("\n");
        for (String tnc : termsAndConditions) {
            Paragraph paragraph = createParagraph(tnc, getFontRobotoForContent(M_FONT_SIZE, Font.NORMAL, BLACK_COLOUR));
            document.add(paragraph);
        }
    }

    private float createInventoryTable(Document document, PdfWriter writer, int xCoord, int yCoord,
            DealInvoice dealInvoice) throws DocumentException, IOException {
        PdfPTable table = new PdfPTable(7);

        return 0;
    }

    private float createLineItemTable(Document document, PdfWriter writer, float xCoord, float yCoord,
            DealInvoice dealInvoice) throws DocumentException, IOException {
        PdfContentByte canvas = writer.getDirectContent();
        PdfPTable lineItemTable = createTable(7, 100, new int[] { 2, 1, 1, 1, 1, 1, 1 });
        lineItemTable.setTotalWidth(550);

        String lineItemInventoryNameHeader = String.format("%s Name",
                DealType.OPEN_AUCTION.equals(dealInvoice.getDealType()) ? "Billboard" : "Line Item");
        List<String> lineItemHeaderNames = Arrays.asList(lineItemInventoryNameHeader, "Impression Type", "Date & TIme",
                "CPM Rate", "Total Potential View", "Total Spot", "Total Earning");

        // create table header
        for (String headerName : lineItemHeaderNames) {
            Phrase headerNamePhrase = createPhrase(headerName, getFontRobotoForHeading(S_FONT_SIZE, Font.NORMAL));
            PdfPCell headerNameCell = createHeaderCell(headerNamePhrase);
            lineItemTable.addCell(headerNameCell);
        }

        for (DealInvoiceLineItemDTO lineOrInventoryItem : dealInvoice.getLineItems()) {

            String lineItemInventoryName = (DealType.OPEN_AUCTION.equals(dealInvoice.getDealType()))
                    ? String.join(", ", lineOrInventoryItem.getBillboardNames())
                    : lineOrInventoryItem.getLineItemName();

            List<String> lineItemFields = Arrays.asList(lineItemInventoryName,
                    lineOrInventoryItem.getImpressionType().toString(), "Date & Time",
                    String.valueOf(lineOrInventoryItem.getCpmRate()),
                    String.valueOf(lineOrInventoryItem.getTotalPotentialView()),
                    String.valueOf(lineOrInventoryItem.getTotalSpot()),
                    String.valueOf(lineOrInventoryItem.getTotalEarning() + " " + dealInvoice.getCurrency()));

            for (String lineItemField : lineItemFields) {

                Phrase lineItemPhrase = createPhrase(lineItemField,
                        getFontRobotoForContent(S_FONT_SIZE, Font.NORMAL, BLACK_COLOUR));
                PdfPCell lineItemCell = createLineItemTableCell(lineItemPhrase);
                lineItemCell.setVerticalAlignment(Element.ALIGN_CENTER);
                lineItemCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                lineItemCell.setPaddingLeft(9);
                lineItemTable.addCell(lineItemCell);

            }

        }

        float top = document.top() - document.topMargin();
        float yPos = top - 200;
        int start = 0;
        int stop = 0;
        for (int i = 0; i < dealInvoice.getLineItems().size(); i++) {
            yPos -= lineItemTable.getRowHeight(i);
            if (yPos < 0) {
                stop = --i;
                lineItemTable.writeSelectedRows(0, 12, start, stop, xCoord, yCoord, canvas);
                document.newPage();
                start = stop;
                yPos = top;
            }
        }
        // for the new page
        lineItemTable.writeSelectedRows(0, 12, stop, -1, xCoord, top, canvas);
        return yPos; // use to position (y Coordinate) next item in page
    }

    private void createTopSummaryTable(PdfContentByte pcb, Map<String, String> items, int xCoord, int yCoord)
            throws DocumentException, IOException {

        PdfPTable topSummaryTable = createTable(2, 60, new int[] { 1, 1 });
        topSummaryTable.setTotalWidth(250);
        topSummaryTable.setHorizontalAlignment(Element.ALIGN_LEFT);

        for (Map.Entry<String, String> entry : items.entrySet()) {
            try {
                Phrase left1 = createPhrase(entry.getKey(),
                        getFontOpenSansForContent(S_FONT_SIZE, Font.NORMAL, GREYISH_COLOUR));
                PdfPCell cell1 = new PdfPCell(left1);
                cell1.setUseVariableBorders(true);
                cell1.setBorderColor(WHITE_COLOUR);
                cell1.setPaddingTop(2f);
                cell1.setPaddingBottom(2f);
                cell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
                cell1.setFixedHeight(21f);
                topSummaryTable.addCell(cell1);

                Phrase left2 = createPhrase(entry.getValue(),
                        getFontOpenSansForContent(S_FONT_SIZE, Font.NORMAL, BLUEISH_COLOUR));
                PdfPCell cell2 = new PdfPCell(left2);
                cell2.setUseVariableBorders(true);
                cell2.setBorderColor(WHITE_COLOUR);
                cell2.setBorderColorBottom(BLUEISH_COLOUR);
                cell2.setPaddingTop(2f);
                cell2.setPaddingBottom(2f);
                cell2.setFixedHeight(21f);
                cell2.setVerticalAlignment(Element.ALIGN_BOTTOM);
                topSummaryTable.addCell(cell2);
            } catch (DocumentException | IOException e) {
                e.printStackTrace();
            }
        }

        topSummaryTable.writeSelectedRows(0, -1, xCoord, yCoord, pcb);
    }

    private Document addMetaData(Document document, DealInvoice dealInvoice) {
        document.addTitle(String.format("DEAL INVOICE - %s", dealInvoice.getInvoiceId()));
        document.addSubject(String.format("Invoice report for %s", dealInvoice.getMonthAndYear()));
        document.addKeywords("PDF, Deal, Invoice, Programmatic");
        document.addAuthor("LMX");
        document.addCreator("LMX");
        return document;
    }

    // Deal invoice Helper functions
    private List<Map<String, String>> prepareTopSummaryKeyValue(DealInvoice dealInvoice) {
        List<Map<String, String>> preparedData = new ArrayList<>();

        Map<String, String> leftItems = new LinkedHashMap<>();
        leftItems.put("DEMAND SIDE PLATFORM : ",
                dealInvoice.getBuyers() != null ? dealInvoice.getBuyers().get(0).getDsp() : "");
        leftItems.put("BUYER NAME : ", dealInvoice.getBuyers() != null ? dealInvoice.getBuyers().get(0).getName() : "");
        leftItems.put("BUYER SEAT ID : ",
                dealInvoice.getBuyers() != null ? dealInvoice.getBuyers().get(0).getSeatId() : "");
        leftItems.put("BILLING NAME : ", dealInvoice.getBillingName() != null ? dealInvoice.getBillingName() : "");
        leftItems.put("BILLING ADDRESS : ",
                dealInvoice.getBillingAddress() != null ? dealInvoice.getBillingAddress() : "");

        Map<String, String> rightItems = new LinkedHashMap<>();
        rightItems.put("DEAL NAME : ", dealInvoice.getName() != null ? dealInvoice.getName() : "");
        rightItems.put("DEAL ID : ", dealInvoice.getDealId() != null ? dealInvoice.getDealId() : "");
        String formattedDate = DateTimeFormatter.ofPattern("d MMM yyyy").format(dealInvoice.getCreatedDate());
        rightItems.put("DATE : ", dealInvoice.getCreatedDate() != null ? formattedDate : "");
        rightItems.put("CREATED BY : ", dealInvoice.getCreatedBy() != null ? dealInvoice.getCreatedBy() : "");
        rightItems.put("STATUS : ", dealInvoice.getStatus() != null ? dealInvoice.getStatus().toString() : "");

        preparedData.add(leftItems);
        preparedData.add(rightItems);
        return preparedData;
    }

    // PDF Helper functions
    private void createCanvasTextHeader(PdfContentByte canvas, int fontSize, float x, float y, String text,
            BaseColor color) throws DocumentException, IOException {
        canvas.setFontAndSize(getBaseFontRoboto(), fontSize);
        canvas.setColorFill(color);
        canvas.beginText();
        canvas.showTextAligned(Element.ALIGN_LEFT, text, x, y, 0);
        canvas.endText();
    }

    private void createColumnText(PdfContentByte canvas, float xCoord, float yCoord, String textContent, Font font)
            throws DocumentException {
        Paragraph paragraph = createParagraph(textContent, font);
        ColumnText ct = new ColumnText(canvas);
        ct.setSimpleColumn(xCoord + 10, yCoord - 1, 570, yCoord - 170);
        ct.addElement(paragraph);
        ct.go();
    }

    private PdfPTable createTable(int columnNumbers, int tableWidth, int[] columnWidths) throws DocumentException {
        PdfPTable table = new PdfPTable(columnNumbers);
        table.setWidthPercentage(tableWidth);
        table.setWidths(columnWidths);
        return table;
    }

    public Paragraph createParagraph() {
        return new Paragraph();
    }

    public Paragraph createParagraph(String input, Font font) {
        return new Paragraph(input, font);
    }

    public Phrase createPhrase(String input, Font font) {
        return new Phrase(input, font);
    }

    public Phrase createPhrase(String input) {
        return new Phrase(input);
    }

    public PdfPCell createHeaderCell(Phrase phrase) {
        PdfPCell cell = new PdfPCell(phrase);
        cell.setPaddingBottom(5);
        cell.setPaddingTop(4);
        cell.setPaddingLeft(8);
        cell.setBorder(Rectangle.BOTTOM);
        cell.setBackgroundColor(getBackgroundColor());
        return cell;
    }

    public PdfPCell createLineItemTableCell(Phrase phrase) throws DocumentException, IOException {
        phrase.setFont(getFontRobotoForHeading(10, Font.NORMAL));

        PdfPCell cell = createCell(Rectangle.NO_BORDER, phrase);
        cell.setBackgroundColor(getBackgroundColor());
        cell.setFixedHeight(30f);
        cell.setPaddingTop(5f);
        cell.setPaddingBottom(10);
        cell.setUseVariableBorders(true);
        cell.setBorder(Rectangle.BOTTOM);
        cell.setBorderColor(GREYISH_COLOUR);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);

        return cell;
    }

    private Paragraph addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
        return paragraph;
    }

    private Image getLmxImage() throws IOException, BadElementException {
        return Image.getInstance(getLogoResource().getURI().toString());
    }

    private Resource getLogoResource() {
        return loader.getResource("classpath:images/red-LMX-v3.png");
    }

    private Resource getRobotoFontResource() {
        return loader.getResource("classpath:fonts/Roboto-Regular.ttf");
    }

    private Resource getOpenSansFontResource() {
        return loader.getResource("classpath:fonts/OpenSans-Regular.ttf");
    }

    private BaseFont getBaseFontRoboto() throws DocumentException, IOException {
        return BaseFont.createFont(getRobotoFontResource().getURI().toString(), BaseFont.WINANSI, BaseFont.EMBEDDED);
    }

    private BaseFont getBaseFontOpenSans() throws DocumentException, IOException {
        return BaseFont.createFont(getOpenSansFontResource().getURI().toString(), BaseFont.WINANSI, BaseFont.EMBEDDED);
    }

    public Font getFontRobotoForContent(int size, int fontWeight, BaseColor baseColor)
            throws DocumentException, IOException {
        return new Font(getBaseFontRoboto(), size, fontWeight, baseColor);
    }

    public BaseColor getBackgroundColor() {
        return new BaseColor(255, 255, 255);
    }

    public Font getFontOpenSansForContent(int size, int fontWeight, BaseColor baseColor)
            throws DocumentException, IOException {
        return new Font(getBaseFontOpenSans(), size, fontWeight, baseColor);
    }

    public Font getFontRobotoForHeading(int size, int fontWeight) throws DocumentException, IOException {
        return new Font(getBaseFontRoboto(), size, fontWeight, getFontColorHeading());
    }

    private static BaseColor getFontColorHeading() {
        return RED_COLOUR;
    }

    public PdfPCell createCell(int border, Paragraph paragraph) {
        PdfPCell cell = new PdfPCell();
        cell.setBorder(border);
        cell.addElement(paragraph);
        return cell;
    }

    private PdfPCell createCell(int border, Phrase phrase) {
        PdfPCell cell = new PdfPCell();
        cell.setBorder(border);
        cell.addElement(phrase);
        return cell;
    }

}
