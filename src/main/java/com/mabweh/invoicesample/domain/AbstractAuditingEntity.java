package com.mabweh.invoicesample.domain;

import java.time.ZonedDateTime;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.Data;

@Data
public abstract class AbstractAuditingEntity {
	
	@CreatedBy
	@Field("created_by")
	// @JsonIgnore
	private String createdBy;
	
	@CreatedDate
	@Field("created_date")
	// @JsonIgnore
	private ZonedDateTime createdDate;
	
    @LastModifiedBy
    @Field("last_modified_by")
    // @JsonIgnore
    private String lastModifiedBy;
    
	@LastModifiedDate
	@Field("last_modified_date")
    // @JsonIgnore
	private ZonedDateTime lastModifiedDate;
}
