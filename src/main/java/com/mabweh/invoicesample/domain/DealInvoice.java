package com.mabweh.invoicesample.domain;

import java.util.List;
import java.io.Serializable;
import java.time.ZonedDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.mabweh.invoicesample.enums.CreativeStatus;
import com.mabweh.invoicesample.enums.DealStatus;
import com.mabweh.invoicesample.enums.DealType;
import com.mabweh.invoicesample.enums.RateType;
import com.mabweh.invoicesample.dto.CustomDate;
import com.mabweh.invoicesample.dto.BuyerDTO;
import com.mabweh.invoicesample.dto.CountryDTO;
import com.mabweh.invoicesample.dto.DealInvoiceInsightsDTO;
import com.mabweh.invoicesample.dto.DealInvoiceLineItemDTO;
import com.mabweh.invoicesample.dto.DealInvoicePriceDTO;
import com.mabweh.invoicesample.dto.SellerDTO;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@Document(collection = "ssp_deal_invoice")
public class DealInvoice extends AbstractAuditingEntity implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    private String invoiceId;
    private String monthAndYear; // YYYY-MM

    private String name;
    private String dealId;
    private String proposalId;
    private DealType dealType; // base on deal type, may have to fetch different lineItem

    private double netCost;
    private String currency;
    private RateType rateType;
    private CustomDate startDate;
    private CustomDate endDate;
    private CreativeStatus creativeStatus;
    private DealStatus status;
    private String billingName;
    private String billingAddress;
    private String termsAndConditions;
    
    private List<DealInvoiceLineItemDTO> lineItems;
    private DealInvoiceInsightsDTO insightsDetails;
    private DealInvoicePriceDTO priceDetails;

    private String accountCompanyId;
    private CountryDTO country;
    private List<BuyerDTO> buyers;
    private SellerDTO seller;

    private long deliveryGoal;
    private long potentialViews;

	private String createdBy;
	private ZonedDateTime createdDate;

}
